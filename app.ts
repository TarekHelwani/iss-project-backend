import { Server as HttpServer } from "http";
import { Server, ServerOptions } from "socket.io";
import { ClientEvents } from "./src/app/utils/events";
import { PasswordService } from "./src/app/services/passwords.service";
import { UserService } from "./src/app/services/users.service";
import { HandshakeService } from "./src/app/services/handshake.service";
import { ShareRequestService } from "./src/app/services/shareRequest.service";
const path = require('path')
import { setSocketId } from './src/app/utils/cache'

export function createApplication (
    httpServer: HttpServer,
    serverOptions: Partial<ServerOptions> = {}
): Server<ClientEvents> {
    const io = new Server<ClientEvents>(httpServer, serverOptions);

    io.on("connection", (socket) => {
        console.log('The Client server has connected .. ')
        setSocketId(socket.id)
        socket.on("user:register", UserService.register);
        socket.on("user:login", UserService.login);
        socket.on("user:logout", UserService.logout);
        socket.on("user:list", UserService.findAll);
        socket.on("password:create", PasswordService.create);
        socket.on("password:find", PasswordService.findById);
        socket.on("password:update", PasswordService.update);
        socket.on("password:delete", PasswordService.delete);
        socket.on("password:list", PasswordService.findAll);
        socket.on("handshake:public_key", HandshakeService.sendPublicKey );
        socket.on("handshake:session_key", HandshakeService.confirmSessionKey );
        socket.on("handshake:session_key", HandshakeService.confirmSessionKey );
        socket.on("shareRequest:list", ShareRequestService.getShareRequests );
        socket.on("shareRequest:share", ShareRequestService.sharePassword );
        socket.on("shareRequest:public_key", ShareRequestService.sendPublicKey );
    });

    return io;
}