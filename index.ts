import { createServer } from "http";
import { createApplication } from "./app";
import { connectToDb } from "./src/app/connectToDb"
import { RequestManager } from './src/app/requests/requestManager'
import { Symmetric } from './src/app/security/symmetric'
import { Asymmetric } from './src/app/security/asymmetric'
import { DigitalSignature } from './src/app/security/digitalSignature'
import {AES} from "crypto-ts";
import config = require("config");
import { cache } from "./src/app/utils/cache";

const httpServer = createServer();
connectToDb().then(r => console.log('Connected to the database .. '))

export const requestManager = new RequestManager(new Asymmetric())



createApplication(
    httpServer,
    {
        cors: {
            origin: ["http://localhost:4200"],
        },
    }
);

console.log('The server is running on port ' + config.get('port'))
httpServer.listen(config.get('port'));


