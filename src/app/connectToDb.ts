import { connect } from 'mongoose'

export async function connectToDb(): Promise<void> {
    await connect('mongodb://localhost:27017/iss')
}