import { Schema } from "mongoose";
import * as mongoose from "mongoose";

export type PasswordID = string;

export type Password = {
    _id: PasswordID;
    userId: string;
    address: string;
    email: string;
    password: string;
    description: string;
    attachments: string;
}

const schema = new Schema<Password>({
    address: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    description: {
        type: String
    },
    attachments: {
        type: String
    },
    userId: {
        type: Schema.Types.ObjectId, ref: 'User'
    },
})

export const PasswordModel = mongoose.model('Password', schema)
