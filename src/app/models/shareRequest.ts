import { Schema } from "mongoose";
import * as mongoose from "mongoose";

export type ShareRequest = {
    sender: string
    receiver: string
    password: any
}

const schema = new Schema({
    sender: {
        type: String
    },
    receiver: {
        type: String
    },
    password: {
        type: Object
    }
})

export const ShareRequestModel = mongoose.model('ShareRequest', schema)
