import { Password } from "./password";
import { Schema } from "mongoose";
import type = Mocha.utils.type;
import * as mongoose from "mongoose";

export type UserID = string;

export type User = {
    _id: UserID;
    email: string;
    password: string;
    passwords: Password[];
    publicKey: any
}

const schema = new Schema<User>({
    email: {
        type: String
    },
    password: {
        type: String
    },
    passwords: [
        { type: Schema.Types.ObjectId, ref: 'Password' }
    ],
    publicKey: {
        type: Object
    }
})

export const UserModel = mongoose.model('User', schema)
