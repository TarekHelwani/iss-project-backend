import { cache, getSocketId } from "../utils/cache";
import { Symmetric } from "../security/symmetric";
import { Asymmetric } from "../security/asymmetric";
import { DigitalSignature } from "../security/digitalSignature";
import { DigitalCertificate } from "../security/digitalCertificate";
import { SecureThePassword } from "../security/secureThePassword";
import { User } from "../models/user";
import { Encryption } from "../security/crypto";
import { UserService } from "../services/users.service";

export class RequestManager {

    private encryptionAlgorithm
    private readonly digitalSignature
    private readonly secureThePassword
    private readonly digitalCertificate

    constructor(encryptionAlgorithm: Encryption) {
        this.encryptionAlgorithm = encryptionAlgorithm
        this.digitalSignature = new DigitalSignature()
        this.secureThePassword = new SecureThePassword()
        this.digitalCertificate = new DigitalCertificate()
    }

    public containSignature(obj: any) {
        return obj && obj.hasOwnProperty("signature")
    }

    public async sendEncryptedData(data: any) {

        if (!cache.get('CSR')) {
            this.digitalCertificate.generate()
        }

        return this.encryptionAlgorithm.encrypt(data)
    }

    public async getDecryptedData(data: any) {

        if (this.containSignature(data)) {
            const decoder = new TextDecoder()

            const { message: decryptedMessage, signature } = data
            data = decoder.decode(decryptedMessage)
            await this.digitalSignature.decrypt(data, signature, cache.get(getSocketId()).publicKey)
        }

        console.log('data1 => ', data)

        data = (await this.encryptionAlgorithm.decrypt(data))?.data

        console.log('data2 => ', data)

        return {
            data: data
        }
    }
}