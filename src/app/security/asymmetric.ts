import {Encryption} from "./crypto";
import NodeRSA from 'encrypt-rsa';
import {HandshakeService} from '../services/handshake.service'
import {keys, sessionKey, getSocketId} from '../utils/cache'
import rsa from 'js-crypto-rsa';
import {AES, enc} from "crypto-ts";


export class Asymmetric implements Encryption {

    private handshakeService = new HandshakeService()
    private privateKey;
    private publicKey;

    constructor() {
        rsa.generateKey(2048).then((key) => {
            this.publicKey = key.publicKey;
            this.privateKey = key.privateKey;
            keys.set('publicKey', this.publicKey)
            keys.set('privateKey', this.privateKey)
        })
    }

    async encrypt(message: string): Promise<any> {
        // Session key has been confirmed
        return AES.encrypt(JSON.stringify(message), sessionKey.get(getSocketId()))
    }

    async decrypt(message: any): Promise<any> {
        //session key hasn't been confirmed yet
        if (sessionKey.get(getSocketId()) === '') {

            const decodedMessage = await rsa.decrypt(
                message,
                this.privateKey,
                'SHA-256', // optional, for OAEP. default is 'SHA-256'
            );
            const decoder = new TextDecoder();
            const userSessionKey = decoder.decode(decodedMessage)

            sessionKey.set(getSocketId(), userSessionKey)
            console.log('Session Key => ', sessionKey)
            return;
        }

        const bytes = AES.decrypt(message, sessionKey.get(getSocketId()));
        return {
            data: JSON.parse(bytes.toString(enc.Utf8))
        }
    }
}