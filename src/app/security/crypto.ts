export interface Encryption {
    encrypt(message:string): Promise<any>;
    decrypt(message:any): Promise<any>;
}

