import {Encryption} from "./crypto";
const RSA = require('rsa-compat').RSA;
import {cache, getSocketId, keys} from "../utils/cache";
const jwkToPem = require('jwk-to-pem')
import rsa from 'js-crypto-rsa';

export class DigitalCertificate {

    public async generate() {

        const publicAndPrivateKeys = {
            publicKeyJwk: keys.get('publicKey'),
            privateKeyJwk: keys.get('privateKey')
        }

        const certificate = RSA.generateCsrPem(publicAndPrivateKeys, [ 'localhost://5000' ]);
        console.log('==> The CSR <==')
        console.log(certificate);

        cache.set('CSR', certificate)
    }
}