import {Encryption} from "./crypto";
import rsa from "js-crypto-rsa";
import {encode} from "base64-arraybuffer";
import { UserService } from "../services/users.service";


export class DigitalSignature implements Encryption {
    
    encrypt(message: string): Promise<any> {
        return Promise.resolve(undefined);
    }
    
    // this function won't be used
    async decrypt(message: any, signature?: any, publicKey?: any): Promise<any> {
        rsa.verify(
            message,
            signature,
            publicKey,
            'SHA-256'
        ).then((valid) => {
            if (valid) {
                console.log(' >> User has been authorized << ')
            }
            else {
                console.log('User is fake !! .. ')
            }
        });
    }
}