import {Encryption} from "./crypto";
import rsa from "js-crypto-rsa";
import {decode, encode} from "base64-arraybuffer";

export class SecureThePassword implements Encryption {

    async encrypt(message: any, publicKey?: any): Promise<any> {
        
        const encoder = new TextEncoder();

        const decryptedPassword = await rsa.encrypt(
            encoder.encode(JSON.stringify(message.password.password)),
            publicKey,
            'SHA-256',
            // optional, for OAEP. default is 'SHA-256'
        )

        message.password.password = encode(decryptedPassword)

        return message
    }

    decrypt(message: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}