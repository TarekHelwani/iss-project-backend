import {Encryption} from "./crypto";
import {cache, getSocketId} from "../utils/cache";
import {User} from "../models/user";
import { AES, enc, mode, SHA256 } from 'crypto-ts';
import { Response } from '../utils/events';
const hash = require('object-hash')

export class Symmetric implements Encryption {

    async encrypt(message: any): Promise<any> {
        const cachedUser = cache.get(getSocketId())
        // @ts-ignore
        const key = cachedUser.password
        const request = {
            message: JSON.stringify(message),
            ciphertext: hash.MD5(JSON.stringify(message))
        }
        return AES.encrypt(JSON.stringify(request), key).toString()
    }

    async decrypt(message: any): Promise<Response<any>> {
        const cachedUser = cache.get(getSocketId())
        // @ts-ignore
        const key = cachedUser.password
        const bytes = AES.decrypt(message, key);
        const decryptedData = JSON.parse(bytes.toString(enc.Utf8));
        let {message: decryptedMessage, ciphertext} = decryptedData
        decryptedMessage = JSON.parse(decryptedMessage)
        //check if the mac is the same
        const message_authentication_code = hash.MD5(decryptedMessage)
        if (message_authentication_code !== ciphertext) {
            return {
                error: 'MAC is not the same !'
            }
        }
        return {
            data: decryptedMessage
        }
    }
}

