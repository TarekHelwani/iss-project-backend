import * as fs from 'fs';
import { cache, keys } from '../utils/cache';
import { requestManager } from "../../..";


export class HandshakeService {
    
    static async sendPublicKey(callback: (res: any) => void) {
        const publicKey = keys.get("publicKey")
        callback({
            data: publicKey
        })
    }

    static async confirmSessionKey(decryptedMessage:string, callback: (res: any) => void) {
        const decryptedSessionKey = await requestManager.getDecryptedData(decryptedMessage)
        callback({
            data: 'Session key has been confirmed'
        })
    }
}