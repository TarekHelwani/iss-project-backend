import { requestManager } from "../../..";
import {Password, PasswordID, PasswordModel} from "../models/password";
import {User} from "../models/user";
import { ShareRequestModel } from "../models/shareRequest";

import { cache, getSocketId } from "../utils/cache";
import { UserService } from "./users.service";

export class PasswordService {

    static async findAll(callback: (res: any) => void): Promise<void> {
        const cachedUser = cache.get(getSocketId())
        const userId = cachedUser._id
        const passwords: Password[] = await PasswordModel.find({
            userId: userId
        })
        callback({
            data: await requestManager.sendEncryptedData(passwords)
        })
    }

    //password ID
    static async findById(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const id = message.data.passwordId
        const password = await PasswordModel.findById(id)
        callback({
            data: await requestManager.sendEncryptedData(password)
        })
    }

    //Password
    static async create(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const entity = message.data
        const cachedUser = cache.get(getSocketId())
        await PasswordModel.create(entity)
        callback({
            data: 'Password has been created successfully'
        })
    }

    //password
    static async update(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const password = message.data
        await PasswordModel.findByIdAndUpdate(password._id, password)
        callback({
            data: 'Password has been updated successfully'
        })
    }

    //id
    static async delete(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const id = message.data
        await PasswordModel.findByIdAndDelete(id)
        callback({
            data: 'Password has been deleted successfully'
        })
    }
    
}