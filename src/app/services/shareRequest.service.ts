import { requestManager } from "../../..";
import {ShareRequest, ShareRequestModel } from "../models/shareRequest";
import { UserService } from "./users.service";


export class ShareRequestService {

    static async sharePassword(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const request = message.data
        await ShareRequestModel.create(request)
        callback({
            data: 'Reqest has been sent successfully'
        })
    }

    static async getShareRequests(decryptedMessage: string, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const receiverId = message.data

        const shareRequests: ShareRequest[] = await ShareRequestModel.find({
            receiver: receiverId
        })
        callback({
            data: await requestManager.sendEncryptedData(shareRequests)
        })
    }

    static async sendPublicKey(decryptedMessage: any, callback: (res: any) => void): Promise<void> {
        const message = await requestManager.getDecryptedData(decryptedMessage)
        // @ts-ignore
        const request = message.data

        callback({
            data: await requestManager.sendEncryptedData((await UserService.getUserPublicKey(request.receiver)).publicKey)
        })
    }

}