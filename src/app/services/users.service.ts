import {Schema, model} from "mongoose";
import {User, UserID, UserModel} from "../models/user";
import {Response} from "../utils/events";
import {requestManager} from "../../..";
import {cache, getSocketId} from "../utils/cache";

const bcrypt = require('bcrypt');

export class UserService {

    static async register(entity: any, callback: (res: Response<User>) => void): Promise<void> {
        await requestManager.getDecryptedData(entity).then((obj) => {
            entity = obj.data
        })
        entity.password = await bcrypt.hash(entity.password, 10)
        const user = await UserModel.create(entity)
        cache.set(getSocketId(), user)
        callback({
            data: await requestManager.sendEncryptedData(user)
        })
    }

    static async login(userInfo: any, callback: (res: Response<User>) => void): Promise<void> {
        await requestManager.getDecryptedData(userInfo).then((obj) => {
            userInfo = obj.data
        })

        const hashedPassword = await bcrypt.hash(userInfo.password, 10)

        let user = await UserModel.findOne({
            email: userInfo.email,
        })
        if (!user) {
            callback({
                error: 'User not found'
            })
        }
        else {
            if (!bcrypt.compare(user.password, hashedPassword)) {
                user = undefined
            }

            if (!user) {
                callback({
                    error: 'User not found'
                })
            }
            else {
                cache.set(getSocketId(), user)
                callback({
                    data: await requestManager.sendEncryptedData(user)
                })
            }
        }
    }


    static async logout(callback: (res: Response<UserID>) => void): Promise<void> {
        await cache.delete("currentUser")
        callback({
            data: 'You have been logged out'
        })
    }

    static async findAll(callback: (res: any) => void): Promise<void> {
        const users: User[] = await UserModel.find().select(['_id', 'email', 'publicKey'])
        callback({
            data: await requestManager.sendEncryptedData(users)
        })
    }

    static async getUserPublicKey(userId: UserID) {
        return await UserModel.findById(userId, 'publicKey')
    }
}